# go lang unit test
- go test all file _test in package
- go test -v show list function tested  in package
- go test -v -run=[name function test] for one unit test specific  in package
- go test ./... unit test in main root
- go test -v ./...
- go test -v -run={NOMatchTest/{NameBench}} -bench=. run benchmark 

