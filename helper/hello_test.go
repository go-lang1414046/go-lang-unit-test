package helper

import (
	"fmt"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func BenchmarkTableHello(b *testing.B) {
	benchmarks := []struct {
		name    string
		request string
	}{
		{
			name:    "Nurdin",
			request: "Nurdin",
		},
		{
			name:    "Nurdin",
			request: "Nurdin",
		},
	}
	for _, benchmark := range benchmarks {
		b.Run(benchmark.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				Hello(benchmark.request)
			}
		})
	}
}
func BenchmarkSubHello(b *testing.B) {
	b.Run("Nurdin", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Hello("Nurdin")
		}
	})

	b.Run("Rolis", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Hello("Rolis")
		}
	})

}

func BenchmarkHello(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Hello("Nurdin")
	}
	fmt.Println("BenchmarkHello HelloRolis")

}

func BenchmarkHelloRolis(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Hello("Rolis")
	}
	fmt.Println("BenchmarkHelloRolis")
}

func TestSkip(t *testing.T) {
	if runtime.GOOS == "windows" {
		t.Skip("Can't run os windows")
	}
	fmt.Println("TestSkip")
}

func TestHelloAssert(t *testing.T) {
	result := Hello("Nurdin")
	assert.Equal(t, "Hello Nurdin", result, "Result must be Hello Nurdin")
	fmt.Println("TestHelloAssert")

}

func TestHelloRequire(t *testing.T) {
	result := Hello("Nurdin")
	require.Equal(t, "Hello Nurdin", result, "Result must be Hello Nurdin")
	fmt.Println("TestHelloRequire")

}

func ErrorTestHello(t *testing.T) {
	result := Hello("Nurdin")
	if result != "Hello Nurdin" {
		/*
			tidak baik digunakan karena akan tidak akan menjalan code setelahnya
		*/
		t.Error("Result is not Hello Nurdin Error")
	}
	fmt.Println("ErrorTestHello")

}

func FatalTestHello(t *testing.T) {
	result := Hello("Nurdin")
	if result != "Hello Nurdi" {
		/*
			tidak baik digunakan karena akan tidak akan menjalan code setelahnya
		*/
		t.Fatal("Result is not Hello Nurdin Fatal")
	}
	fmt.Println("FatalTestHello")

}

func PanicTestHello(t *testing.T) {
	result := Hello("Nurdin")
	if result != "Hello Nurdin" {
		/*
			tidak baik digunakan karena akan tidak akan menjalan code setelahnya
		*/
		panic("Result is not Hello Nurdin Panic")
	}
	fmt.Println("PanicTestHello")

}

func FailTestHello(t *testing.T) {
	result := Hello("Rolis")
	if result != "Hello Rolis" {
		/*
			code dibawahnya dieksekusi
		*/
		t.Fail()
	}
	fmt.Println("FailNowTestHello")

}

func FailNowTestHello(t *testing.T) {
	result := Hello("Salim")
	if result != "Hello Salim" {
		/*
			tidak baik digunakan karena akan tidak akan menjalan code setelahnya
		*/
		t.FailNow()
	}

	fmt.Println("FailNowTestHello")
}

func TestSubTest(t *testing.T) {
	t.Run("Sub Nurdin", func(t *testing.T) {
		result := Hello("Sub Nurdin")
		assert.Equal(t, "Hello Sub Nurdin", result, "Result must be Hello Sub Nurdin")
		fmt.Println("TestSubTest")
	})
	t.Run("Sub Rolis", func(t *testing.T) {
		result := Hello("Sub Rolis")
		require.Equal(t, "Hello Sub Rolis", result, "Result must be Hello Sub Rolis")
		fmt.Println("TestSubTest")
	})

}

func TestTabelHello(t *testing.T) {
	tests := []struct {
		name     string
		request  string
		expected string
	}{
		{
			name:     "Nurdin",
			request:  "Nurdin",
			expected: "Hello Nurdin",
		},
		{
			name:     "Rolis",
			request:  "Rolis",
			expected: "Hello Rolis",
		},
	}
	/*
		ignore index for use character _
	*/
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := Hello(test.request)
			require.Equal(t, test.expected, result)
			fmt.Println("TestTabelHello")
		})
	}
}

func TestMain(m *testing.M) {
	fmt.Println("TestMain Before")
	m.Run()
	fmt.Println("TestMain after")
}
